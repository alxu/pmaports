# Maintainer: Yassine Oudjana (Tooniis) <y.oudjana@protonmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-msm8996"
pkgname=linux-$_flavor
pkgver=5.14_rc2
pkgrel=0
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8996 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/msm8996-mainline/linux-msm8996"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Source
_tag=v${pkgver//_/-}-msm8996
source="
	linux-msm8996-$_tag.tar.gz::$url/-/archive/$_tag/linux-msm8996-$_tag.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-msm8996-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
}
sha512sums="
e82dd12f90c9cfac4b176e7feb57bea4de232a651f998ab2290eb6609ffa0a3bcf2c55c06d23ded0422e7691180070234539761dd041b6968ab332e23e1fe4bf  linux-msm8996-v5.14-rc2-msm8996.tar.gz
2de664350b482904564aa09ef567856c7dbd2ed6ab1d13aa78e7132598eb481b3fd8a726099bbbbce6a1ffb268020fb98b1da97528f650b06543c8ea87f9dd55  config-postmarketos-qcom-msm8996.aarch64
"
